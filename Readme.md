### Install scrapy

```bash
pip install -r requirements.txt
```

### Run parser
From root of project directory
```bash
python run.py --url URL_1 --max N
```
or 
```bash
python path/run.py --url URL_1 --max N
```
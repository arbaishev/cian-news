import scrapy

class News(scrapy.Item):
    link = scrapy.Field()
    title = scrapy.Field()
    date = scrapy.Field(serializer=str)
    summary = scrapy.Field()
    text = scrapy.Field(serializer=str)
    tags = scrapy.Field()
import scrapy
from urllib import parse

class CianSpider(scrapy.Spider):
    name = "cian"
        
    def __init__(self, *args, **kwargs): 
        super(CianSpider, self).__init__(*args, **kwargs) 

        self.start_urls = kwargs.get('start_urls')
        self.previous_page_number = 0

    def start_requests(self):
        url = self.start_urls[0]
        url, first_page_number = self.get_first_page(url)
        previous_page_number = first_page_number
        yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        url_main = 'https://www.cian.ru'
        SET_SELECTOR = '.list-content__item___3NQAu'
        
        for fields in response.selector.css(SET_SELECTOR):
            link = fields.css(
                '.list-item__content___1Ngu0 a::attr(href)').extract()[1]
            link = url_main+link
            yield scrapy.Request(link, callback=self.parse_news)

        url, current_page_number = self.get_next_page(response.url)

        if current_page_number == int(self.settings.get('MAX_PAGE_COUNT')):
            return

        if self.previous_page_number + 1 == current_page_number:
            self.previous_page_number = current_page_number
            yield scrapy.Request(url, callback=self.parse)

    def parse_news(self, response):
    
        SET_SELECTOR = '.post_card___3JQQF'
        text_str = ''
        
        for fields in response.selector.css(SET_SELECTOR):
            tags = fields.css(
                    '.tags___yNH_G a::attr(href)').extract()
            formatted_tags = [_[14:] for _ in tags]
            
            for p in fields.css('.post-card-content__text___dSyJr p::text'):
                text_str += p.extract() + ' '

            yield {
                'link': response.url,
                'title': fields.css(
                    '.post-card-title___2_ZUj::text').extract()[0],
                'date': fields.css(
                    '.post-card-info__date___2l6I0::text').extract()[0],
                'summary': fields.css(
                    '.post-card-content__summary___23v6e::text').extract()[0],
                'text': text_str.strip(),
                'tags': formatted_tags,
            }

    @staticmethod
    def get_first_page(url):
        url_parts = list(parse.urlparse(url))
        if url_parts[4]:
            query = dict(parse.parse_qsl(url_parts[4]))
            p = int(query.pop('page', 1))
            return (parse.urlunparse(url_parts), p)
        else:
            return (url, 1)

    @staticmethod
    def get_next_page(url):
        """Returns (url, current_page_number)."""
        url_parts = list(parse.urlparse(url))
        query = dict(parse.parse_qsl(url_parts[4]))
        p = int(query.pop('page', 1)) + 1
        query.update({'page': p})
        url_parts[4] = parse.urlencode(query)
        return (parse.urlunparse(url_parts), p - 1)

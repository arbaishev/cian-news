from rotating_proxies.middlewares import RotatingProxyMiddleware
import re

class RotatingProxyMiddlewareCustom:
    def _handle_result(self, request, spider):
        proxy = self.proxies.get_proxy(request.meta.get('proxy', None))
        if not (proxy and request.meta.get('_rotating_proxy')):
            return
        ban = request.meta.get('_ban', None)
        
        if ban is True:
            self.proxies.mark_dead(proxy)
            pattern = re.compile("(?<=\=)[\w\d\:\/\.\-]+")
            m = pattern.findall(request.url)
            if m:
                request = request.replace(url=m[0])
                return self._retry(request, spider)
            else:
                return self._retry(request, spider)
        elif ban is False:
            self.proxies.mark_good(proxy)
            
    RotatingProxyMiddleware._handle_result = _handle_result
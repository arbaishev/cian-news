BOT_NAME = 'scrapy_cian'

USER_AGENT = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.93 Safari/537.36"

SPIDER_MODULES = ['scrapy_cian.spiders']
NEWSPIDER_MODULE = 'scrapy_cian.spiders'

DOWNLOAD_DELAY = 3
# Disable cookies (enabled by default)
COOKIES_ENABLED = False

# Proxy list containing entries like
# http://host1:port
# https://host2:port
# ...
ROTATING_PROXY_LIST_PATH  = 'scrapy_cian/proxy.list'

DOWNLOADER_MIDDLEWARES = {
    'rotating_proxies.middlewares.RotatingProxyMiddleware': 610,
    'rotating_proxies.middlewares.BanDetectionMiddleware': 620,
    'scrapy_cian.middlewares.RotatingProxyMiddlewareCustom': 600,
}

ITEM_PIPELINES = {
    'scrapy_cian.pipelines.CianProjectPipeline':100,
}

MAX_PAGE_COUNT = 500

LOG_ENABLED = True
LOG_LEVEL = 'INFO' # Levels: CRITICAL, ERROR, WARNING, INFO, DEBUG
LOG_FILE = 'output/logfile.log'
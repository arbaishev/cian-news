from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from scrapy import signals
import time
import json
import os
import sys
import argparse

def run_spider(start_urls, max_pages):
    runner = CrawlerProcess(get_project_settings())
    runner.settings.set('MAX_PAGE_COUNT', max_pages, priority='cmdline')
    runner.crawl('cian', start_urls=start_urls)
    runner.start()

def print_result():
    tracks = []
    time.sleep(5) 
    with open('output/scraped_data_utf8.json', 'r+', encoding='utf-8') as t:
        for line in t:
            tracks.append(json.loads(line))
    print(tracks)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--url',
                            nargs='+',
                            default=["https://www.cian.ru/novosti/"])

    parser.add_argument('--max',
                            nargs='?',
                            default=500)
    args = parser.parse_args()
    os.chdir(os.path.dirname(os.path.abspath(__file__)))
    run_spider(args.url, args.max)
    print_result()